import 'package:projet1/Les%20Classe/article.dart';

class Categorie {
  int id;
  String nom;
  List<Article> articlesCategorie;
  Categorie({this.id,this.nom,this.articlesCategorie});
}