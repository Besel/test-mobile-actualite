import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:projet1/Les%20Classe/article.dart';
import 'package:projet1/Les%20Classe/categorie.dart';
import 'package:projet1/Variable%20et%20Fonction/list_classe_variable.dart';
import 'package:projet1/affiche%20Cat%20et%20Actu/detail_artiicle.dart';
import 'package:projet1/affiche%20Cat%20et%20Actu/liste_articles_categorie.dart';

// un article
class AfficheUnAriticle extends StatefulWidget {
  final Article article;

  const AfficheUnAriticle({Key key, this.article}) : super(key: key);
  @override
  _AfficheUnAriticleState createState() => _AfficheUnAriticleState();
}

class _AfficheUnAriticleState extends State<AfficheUnAriticle> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
    onTap: (){
      Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailArticle(carticle:widget.article)));
    },
    child: Container(
      child: SingleChildScrollView(
        child: Column(children: [
          Container(
            width: MediaQuery.of(context).size.width*0.3,
            height: MediaQuery.of(context).size.width*0.3,
            child: Image.asset(widget.article.photo,fit: BoxFit.cover,)),
          Text(widget.article.nom,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),)
          
        ],),
      ),
    )
  );
  }
}

//une categorie
class UneCategorie extends StatefulWidget {
  final Categorie categorie;

  const UneCategorie({Key key, this.categorie}) : super(key: key);
  @override
  _UneCategorieState createState() => _UneCategorieState();
}

class _UneCategorieState extends State<UneCategorie> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children:[
          Container(
            decoration: BoxDecoration(
              color:Colors.grey[200],
              
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Nouveauté : "+widget.categorie.nom.toUpperCase(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.purple),),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.all(7),
                        padding: EdgeInsets.all(1),
                        decoration: BoxDecoration(
                        color: Colors.grey[300],
                          border: Border.all(color: Colors.red),
                          borderRadius: BorderRadius.circular(3)
                        ),
                        //color: Colors.grey, 
                        child: Text("voir plus".toUpperCase(),style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold),)), 
                    onTap: (){
                      searchArticlesListe = [];
                      setState(() {
                        nomCategorie = widget.categorie.nom;
                      });
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ArticlesCategorie(categorie:widget.categorie)));
                    })
                ],),
            Container(
              child: TroisDernierArticlesCategorie(mesArticles: widget.categorie.articlesCategorie,),
            ),
              ],
            ),
          ),
          Divider(),
        ]
      ),
      
    );
  }
}

//afficharge les troiis derniers articles de la categorie
class TroisDernierArticlesCategorie extends StatefulWidget {
  final List<Article> mesArticles;

  const TroisDernierArticlesCategorie({Key key, this.mesArticles}) : super(key: key);
  @override
  _TroisDernierArticlesCategorieState createState() => _TroisDernierArticlesCategorieState();
}

class _TroisDernierArticlesCategorieState extends State<TroisDernierArticlesCategorie> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AfficheUnAriticle(article: widget.mesArticles[widget.mesArticles.length-1],),
          AfficheUnAriticle(article: widget.mesArticles[widget.mesArticles.length-2],),
          AfficheUnAriticle(article: widget.mesArticles[widget.mesArticles.length-3],),
        ],
      ),
    );
  }
}

// liste des categorie disponible avec leur trois derniers article
Widget tousCategories(List<Categorie> mesCategories){
  return ListView.builder(
    itemCount: mesCategories.length,
    itemBuilder: (context,index){
      return UneCategorie(categorie:mesCategories[index]);
    });
}


//liste des articles d'une categorie avec gridview
class TousLesArticles extends StatefulWidget {
  final List<Article> articles;

  const TousLesArticles({Key key, this.articles}) : super(key: key);
  @override
  _TousLesArticlesState createState() => _TousLesArticlesState();
}

class _TousLesArticlesState extends State<TousLesArticles> {
  @override
  Widget build(BuildContext context) {
     List<Widget> list = [];
      for (var article in widget.articles) {
        list.add(InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailArticle(carticle:article)));
          },
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:[
                  Container(
                    width: MediaQuery.of(context).size.width*0.25,
                    height: MediaQuery.of(context).size.width*0.3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(article.nom,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),),
                        SizedBox(height:3),
                        Text(article.detail),
                      ],
                    ),
                  ),
                  //ghghgh
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailArticle(carticle:article)));
                    },
                    child: Container(
                      child: SingleChildScrollView(
                        child: Column(children: [
                          Container(
                            width: MediaQuery.of(context).size.width*0.60,
                            height: MediaQuery.of(context).size.width*0.3,
                            child: Image.asset(article.photo,fit: BoxFit.cover,)),
                        ],),
                      ),
                    )
                  )
                  //ghghgh
                ]
              ),
              Divider(),
            ],
          ),
        ));
      }
    return Column(
      children: list
    );
  }
}