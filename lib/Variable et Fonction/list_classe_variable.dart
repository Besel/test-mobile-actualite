import 'package:projet1/Les%20Classe/article.dart';
import 'package:projet1/Les%20Classe/categorie.dart';

List<Article> sportArticles = [
Article(id: 1,nom: "sport1",photo: "asset/sport1.jpg",detail: "le sport de nos jours"),
Article(id: 2,nom: "sport2",photo: "asset/sport2.png",detail: "le sport de nos jours"),
Article(id: 3,nom: "sport3",photo: "asset/sport3.jpg",detail: "le sport de nos jours"),
Article(id: 4,nom: "sport4",photo: "asset/sport4.jpg",detail: "le sport de nos jours"),
Article(id: 5,nom: "sport5",photo: "asset/sport5.jpg",detail: "le sport de nos jours"),
Article(id: 6,nom: "sport6",photo: "asset/sport6.jpg",detail: "le sport de nos jours"),
];

List<Article> santeArticles = [
Article(id: 1,nom: "sante1",photo: "asset/sante1.jpeg",detail: "le sante de nos jours"),
Article(id: 2,nom: "sante2",photo: "asset/sante2.jpg",detail: "le sante de nos jours"),
Article(id: 3,nom: "sante3",photo: "asset/sante3.jpg",detail: "le sante de nos jours"),
Article(id: 4,nom: "sante4",photo: "asset/sante4.jpg",detail: "le sante de nos jours"),
];

List<Article> armeArticles = [
Article(id: 1,nom: "arme1",photo: "asset/arme1.jpg",detail: "le arme de nos jours"),
Article(id: 2,nom: "arme2",photo: "asset/arme2.jpg",detail: "le arme de nos jours"),
Article(id: 3,nom: "arme3",photo: "asset/arme3.jpg",detail: "le arme de nos jours"),
Article(id: 4,nom: "arme4",photo: "asset/arme4.jpeg",detail: "le arme de nos jours"),
Article(id: 5,nom: "arme5",photo: "asset/arme5.jpg",detail: "le arme de nos jours"),
Article(id: 6,nom: "arme6",photo: "asset/arme6.jpg",detail: "le arme de nos jours"),
Article(id: 7,nom: "arme7",photo: "asset/arme7.jpg",detail: "le arme de nos jours"),
];

List<Article> businessArticles = [
Article(id: 1,nom: "business1",photo: "asset/business1.jpg",detail: "le business de nos jours"),
Article(id: 2,nom: "business2",photo: "asset/business2.jpg",detail: "le business de nos jours"),
Article(id: 3,nom: "business3",photo: "asset/business3.jpg",detail: "le business de nos jours"),
Article(id: 4,nom: "business4",photo: "asset/business4.jpeg",detail: "le business de nos jours"),
Article(id: 5,nom: "business5",photo: "asset/business5.jpg",detail: "le business de nos jours"),
];

Categorie sport = Categorie(id: 1,nom: "sport",articlesCategorie: sportArticles);
Categorie sante = Categorie(id: 2,nom: "sante",articlesCategorie: santeArticles);
Categorie arme = Categorie(id: 3,nom: "arme",articlesCategorie: armeArticles);
Categorie business = Categorie(id: 4,nom: "business",articlesCategorie: businessArticles);


List<Categorie> tousCategorie = [sport,sante,arme,business];
List<Categorie> searchCategorie = [];
String nomCategorie;
//liste de recherche articles
List<Article> searchArticlesListe = [];







