import 'package:flutter/material.dart';
import 'package:projet1/Les%20Classe/categorie.dart';
import 'package:projet1/Variable%20et%20Fonction/fonctions.dart';
import 'package:projet1/Variable%20et%20Fonction/list_classe_variable.dart';


class Acceuil extends StatefulWidget {
  @override
  _AcceuilState createState() => _AcceuilState();
}

class _AcceuilState extends State<Acceuil> {

  TextEditingController cSearch = TextEditingController();
  Widget appBarTitle =Text("Bienvenu sur actualité.com");
  
bool search = false;
  var valeur;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: !search ? Icon(Icons.search) : Icon(Icons.search_off), 
            onPressed: (){
              setState(() {
                search = !search;
              });
              if (search) {
                
              setState(() {
                appBarTitle = Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    border: Border.all(color: Colors.grey[600]),
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: TextField(
                    onChanged: (value){
                      setState(() {
                        valeur = value;
                      });
                      // recherche dans les categorie
                      List<Categorie> searchecat = [];
                      for (var cat in tousCategorie) {
                         
                        if (cat.nom.toUpperCase().contains(valeur.toUpperCase())) {
                            searchecat.add(cat);
                        }
                      }
                      searchCategorie = searchecat;
                      setState(() {
                        searchecat = [];
                      });
                    },
                    controller: cSearch,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      prefixIcon: Icon(Icons.search),
                      hintText: "Chercher une Catégorie ...",
                      hintStyle: TextStyle(color: Colors.red),
                      ),
                  ),
                );
              });
              } else {
                setState(() {
                  appBarTitle =Text("Actualité.com");
                });
              }
          })
        ],
        title: appBarTitle,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: tousCategories(search ? searchCategorie : tousCategorie),
        ),
    );
  }
}