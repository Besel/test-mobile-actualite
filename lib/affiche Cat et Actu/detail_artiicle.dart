import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projet1/Les%20Classe/article.dart';

class DetailArticle extends StatefulWidget {
  final Article carticle;

  const DetailArticle({Key key, this.carticle}) : super(key: key);
  @override
  _DetailArticleState createState() => _DetailArticleState();
}

class _DetailArticleState extends State<DetailArticle> {

String detailKabo1 ="Pour un dev mobile, la première étape du recrutement est de participer à ce test :\n";
String detailKabo2 ="    En utilisant Flutter, créez une application mobile permettant de s'informer sur les actualités.\n";
String detailKabo3 ="    L'utilisateur doit pouvoir :\n";
String detailKabo4 =" - Obtenir les dernières actualités à partir d'un fil d'actualité. \n";
String detailKabo5 =" - Rechercher par catégorie. \n";
String detailKabo6 =" - Consulter les différents articles par catégorie, par exemple ";
String detailKabo7 ="Affaire, Santé, Business, etc \n\n\n";
String detailKabo8 ="NB : ";
String detailKabo9 ="La liste d'actualité est statique \n\n";
String detailKabo10 ="L'objectif de ce test n'est pas de forcément retourner un projet parfaitement fonctionnel. Le but est de voir votre capacité d'adaptation et l'attention aux détail. La durée maximum alouée pour le test est de ";
String detailKabo11 ="7 jours. \n\n\n";
String detailKabo12 ="Vous pouvez mettre votre code source sur un dépos public ";
String detailKabo13 ="GitLab ";
String detailKabo14 ="puis nous envoyer les deux liens \n\n\n";
String detailKabo15 ="Ps : ";
String detailKabo16 ="Nous n'espérons pas que vous finissez le projet alors donc n'hésitez pas à soumettre votre code quelque soit son état et le niveau d'avancée.";


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close), 
          onPressed: (){
            Navigator.pop(context);
          }),
        title: Text("Article : "+widget.carticle.nom),
        ),
        body: Container(
          padding: EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:[
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset(widget.carticle.photo)),
                Divider(),
                Text("Les détails :",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.purple,fontSize: 18)),
                Divider(),
                //afficharge du message de test
                Container(
                  padding: EdgeInsets.all(3),
                  color: Colors.grey[200],
                  child: RichText(
                    text: TextSpan(
                      text: '',
                      //style: DefaultTextStyle.of(context).style,
                      children: <TextSpan>[
                        TextSpan(text: detailKabo1, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo2, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo3, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo4, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo5, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo6, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo7, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue,fontSize: 16)),
                        TextSpan(text: detailKabo8, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red,fontSize: 16)),
                        TextSpan(text: detailKabo9, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo10, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo11, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue,fontSize: 16)),
                        TextSpan(text: detailKabo12, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo13, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue,fontSize: 16)),
                        TextSpan(text: detailKabo14, style: TextStyle(color: Colors.black,fontSize: 16)),
                        TextSpan(text: detailKabo15, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red,fontSize: 16)),
                        TextSpan(text: detailKabo16, style: TextStyle(color: Colors.black,fontSize: 16)),
                      ],
                    ),
                  ),
                ),
              ]
            ),
          ),
        ),
    );
  }
}