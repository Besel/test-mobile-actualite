import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projet1/Les%20Classe/article.dart';
import 'package:projet1/Les%20Classe/categorie.dart';
import 'package:projet1/Variable%20et%20Fonction/fonctions.dart';
import 'package:projet1/Variable%20et%20Fonction/list_classe_variable.dart';

class ArticlesCategorie extends StatefulWidget {
  final Categorie categorie;

  const ArticlesCategorie({Key key, this.categorie}) : super(key: key);
  @override
  _ArticlesCategorieState createState() => _ArticlesCategorieState();
}

class _ArticlesCategorieState extends State<ArticlesCategorie> {

  TextEditingController cSearchArticle = TextEditingController();
  Widget widgetArticleSearch =Text("T"+"OUS LES ARTICLES ".toLowerCase(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.purple,fontSize: 22));
  
  bool articleSearch = false;
  var valeurArticle;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.close), onPressed: (){
          Navigator.pop(context);
        }),
        title: Text("Catégorie : "+ nomCategorie),),
      body: Container(
        margin: EdgeInsets.all(8),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children:[
              //tousLesArticle(widget.categorie.articlesCategorie)
              Text("Les trois dernier articles",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey[600],fontSize: 18)),
              Divider(),
              TroisDernierArticlesCategorie(mesArticles: widget.categorie.articlesCategorie,),
              Divider(),
              //la bare de recherche
              Container(
                color: Colors.grey[200],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  //Text("Tous les Articles",style:TextStyle(fontWeight: FontWeight.bold)),
                  widgetArticleSearch,
                  //search
                  IconButton(
                    icon: !articleSearch ? Icon(Icons.search) : Icon(Icons.search_off), 
                    onPressed: (){
                      setState(() {
                        articleSearch = !articleSearch;
                      });
                      if (articleSearch) {
                        
                      setState(() {
                        widgetArticleSearch = Container(
                          width: MediaQuery.of(context).size.width*0.75,
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            border: Border.all(color: Colors.grey[600]),
                            borderRadius: BorderRadius.circular(20)
                          ),
                          child: TextField(
                            onChanged: (value){
                              setState(() {
                                valeurArticle = value;
                              });
                              // recherche dans les categorie
                              List<Article> searcheArti = [];
                              for (var cat in widget.categorie.articlesCategorie) {
                                
                                if (cat.nom.toUpperCase().contains(valeurArticle.toUpperCase())) {
                                    searcheArti.add(cat);
                                }
                              }
                              searchArticlesListe = searcheArti;
                              setState(() {
                                searcheArti = [];
                              });
                            },
                            controller: cSearchArticle,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(Icons.search),
                              hintText: "Chercher un article ...",
                              hintStyle: TextStyle(color: Colors.red),
                              ),
                          ),
                        );
                      });
                      } else {
                        searchArticlesListe = [];
                        setState(() {
                          widgetArticleSearch =Text("T"+"OUS LES ARTICLES ".toLowerCase(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.purple,fontSize: 22));
                        });
                      }
                  })
                ],),
              ),
              Divider(),
              // liste de tous les articles
              TousLesArticles(articles: articleSearch ? searchArticlesListe: widget.categorie.articlesCategorie,),
             
            ]
          ),
        ),
      ),
    );
  }
}